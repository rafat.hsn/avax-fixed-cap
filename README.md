# AVALANCHE Fixed Cap Asset Tutorial

Avalanche 1.0.4 Fuji Testnet Node running locally using the following command

`./avalanchego --network-id=fuji`

Address used to create and send fixed cap asset :

`X-fuji1sh27zhy8cau2ecy285604v8v2ptyfkmseydgmu`

_(Please click on images to view full details)_

**Get Initial Balance**

![Inline image](/img/initbalance.png)

Balance : 1 AVAX

**Create 10,000,000 Units of Fixed Cap Asset XRTK**

![Inline image](/img/createfixedasset.png)

**Check Asset Balance**

![Inline image](/img/initassetbalance.png)

10,000,000 Units of XRTK created.

Asset ID :

`2YKDTYqA6RiUjdkBe8L1dCZKTR3hX6AMfMzdphy6revfGYYpY8`

**Send 100 Units of XRTK to another address**

![Inline image](/img/sendasset.png)

**Transaction Details (AVA Testnet Explorer)**

https://explorer.avax-test.network/tx/RjaDZ72yQFw8RCKqEUWcRf8angFTQPfLpd4puaWQKhdFLyZfX


![Inline image](/img/txdetails.png)

![Inline image](/img/txstatus.png)

**Check Sender Balances**

AVAX Balance :

![Inline image](/img/finalbalance.png)

XRTK (Fixed Cap Asset) Balance :

![Inline image](/img/finalassetbalance.png)

**Check Recipient Balance**

![Inline image](/img/toassetbalance.png)

100 XRTK